import axios from 'axios';

const BASE_URL = 'http://localhost:3000';

export {doLogin, getStores, getProduct};

function doLogin(data){
    const url = `${BASE_URL}/oauth/token`;
    return axios.post(url,data).then(response => response.data);
}

function getStores() {
    const url = `${BASE_URL}/api/stores`;
    return axios.get(url).then(response => response.data);
}

function getProduct(id) {
    const url = `${BASE_URL}/api/product/${id}`;
    return axios.get(url).then(response => response.data);
}