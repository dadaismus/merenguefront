import axios from 'axios';

const BASE_URL = 'http://shopcart.test';

const ACCESS_TOKEN_KEY = 'access_token';

const CLIENT_ID = '2';
const CLIENT_SECRET = process.env.REACT_APP_CORE_API_KEY;

export {login, logout, requireAuth, setAccessToken, isLoggedIn};

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.timeout = 5000;

function login(data) {
    let apiData = {
        ...data,
        'grant_type':'password',
        'client_id':CLIENT_ID,
        'client_secret':CLIENT_SECRET,
        'scope':'*'
    }
    const url = `${BASE_URL}/oauth/token`;
    return axios.post(url,apiData).then(response => response.data);
}

function logout() {
    clearAccessToken();
}

function requireAuth(nextState, replace) {
    if (!isLoggedIn()) {
        replace({pathname: '/'});
    }
}

function getAccessToken() {
    return localStorage.getItem(ACCESS_TOKEN_KEY);
}

function clearAccessToken() {
    localStorage.removeItem(ACCESS_TOKEN_KEY);
}

// Get and store access_token in local storage
function setAccessToken(accessToken) {
    localStorage.setItem(ACCESS_TOKEN_KEY, accessToken);
}

function isLoggedIn() {
    const Token = getAccessToken();
    return !!Token;
}