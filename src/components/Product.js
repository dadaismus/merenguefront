import React from 'react';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Col } from 'reactstrap';
class Product extends React.Component {
render() {
	 return (
    <Col xs="6">
      <Card>
        <CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt="Card image cap" />
        <CardBody>
          <CardTitle>Nombre Producto</CardTitle>
          <CardSubtitle>Caegoría</CardSubtitle>
          <CardText className="text-right">$299</CardText>
        </CardBody>
      </Card>
    </Col>
    );
  }
}
export default Product;