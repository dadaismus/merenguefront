import React from 'react';
import {Alert} from 'reactstrap';

class FormErrors extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Alert color="danger">
                {Object.keys(this.props.formErrors).map((fieldName, i) => {
                    if (this.props.formErrors[fieldName].length > 0) {
                        return (
                            <p className="m-0" key={i}>{fieldName} {this.props.formErrors[fieldName]}</p>
                        )
                    } else {
                        return '';
                    }
                })}
            </Alert>
        )
    }
}

export default FormErrors;