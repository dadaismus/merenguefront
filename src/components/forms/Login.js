import React from 'react';
import {Button, Form, FormGroup, Label, Input, Container, Col, Row} from 'reactstrap';
import BtnFG from './BtnFG.js';
import {login} from '../../utils/AuthService';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faSpinner from '@fortawesome/fontawesome-free-solid/faSpinner'
import {validateField, errorClass} from "./FormValidator";
import FormErrors from './FormErrors';


class Login extends React.Component {

    constructor(props) {
        super(props);

        this.doLogin = this.doLogin.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            loading: false,
            email: '',
            password: '',
            formErrors: {email: '', password: ''},
            emailValid: false,
            passwordValid: false,
            formValid: true
        };
    }

    handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({[name]: value},
            () => {
                //validateField(name, value, this)
            });
    }

    doLogin() {
        this.setState({
            loading: !this.state.loading
        });
        const data = {
            'username': this.state.email,
            'password': this.state.password
        };
        login(data).then((resp) => {
            console.log(resp);
        }).catch((error) => {
            console.log(error);
        }).then(() => {
            this.setState({
                loading: !this.state.loading
            });
        })
    }

    render() {
        const errors = !this.state.formValid ? (
            <FormErrors formErrors={this.state.formErrors}/>
        ) : ('');
        return (
            <Container className="container h-100">
                <Row className="align-items-center h-100">
                    <Col className="col-10 offset-1">
                        <Form>
                            {errors}
                            <FormGroup className={`${errorClass(this.state.formErrors.email)}`}>
                                <Input type="email" name="email" value={this.state.email}
                                       onChange={this.handleChange}
                                       placeholder="Correo electrónico"/>
                            </FormGroup>
                            <FormGroup className={`${errorClass(this.state.formErrors.password)}`}>
                                <Input type="password" name="password" value={this.state.password}
                                       onChange={this.handleChange} placeholder="Contraseña"/>
                            </FormGroup>
                            <Row>
                                <Col className="text-center">
                                    <Button color="primary" onClick={this.doLogin} disabled={this.state.loading}>Inicia
                                        sesión</Button>
                                </Col>
                            </Row>
                            <a className="">Olvidaste tu Contraseña?</a>
                        </Form>
                    </Col>

                </Row>
            </Container>
        );
    }
}

export default Login;

