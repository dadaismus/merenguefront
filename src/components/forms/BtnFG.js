import React from 'react';
import {Button, Container, Row, Col, Input, propTypes, deprecated, CustomInput, ButtonGroup} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css'


class BtnFG extends React.Component {

    render() {
        return (
            <Container>
                <div>
                    <Row>
                        <Col>
                            <Button color="primary">FACEBOOK</Button>{' '}
                        </Col>
                        <Col>
                            <Button color="primary">GOOGLE</Button>{' '}
                        </Col>
                    </Row>
                </div>
            </Container>
        );
    }
}

export default BtnFG;
