import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Container, Row, Col } from 'reactstrap';
export default class Register extends React.Component {
render() {
    return (
      <Form>
	      <Container>
	      	<Row>
	          <Col xs="6">
	          	<FormGroup>
	        		<Input type="text" name="nombre" id="nombre" placeholder="Nombre" />
	        	</FormGroup>
	          </Col>
	          <Col xs="6">
		        <FormGroup>
		          <Input type="text" name="apellido" id="apellido" placeholder="Apellido" />
		        </FormGroup>
	          </Col>
	        </Row>
	        <Row>
	        <Col>
	        	<FormGroup>
	        		<Input type="email" name="email" id="email" placeholder="Correo electrónico" />
	        	</FormGroup>
	        </Col>
	        </Row>
	        <Row>
	        <Col>
		        <FormGroup>
		          <Input type="password" name="password" id="password" placeholder="Contraseña" />
		        </FormGroup>
	        </Col>
	        </Row>
	        <Row>
	        <Col>
		        <FormGroup>
		          <Input type="password" name="password" id="password" placeholder="Confirmar contraseña" />
		        </FormGroup>
		    </Col>
	        </Row>
	        <Row>
	          <Col xs="6" sm="4"></Col>
	          <Col xs="6" sm="4"><Button>Iniciar sesión</Button></Col>
	          <Col sm="4"></Col>
	        </Row>
	      </Container>  
      </Form>
    );
  }
}