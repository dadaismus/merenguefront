export {validateField, errorClass};

function validateField(fieldName, value, component) {
    let fieldValidationErrors = component.state.formErrors;
    let emailValid = component.state.emailValid;
    let passwordValid = component.state.passwordValid;

    switch(fieldName) {
        case 'email':
            emailValid = value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i);
            fieldValidationErrors.email = emailValid ? '' : ' no es válido';
            break;
        case 'password':
            passwordValid = value.length >= 6;
            fieldValidationErrors.password = passwordValid ? '': ' es muy corta';
            break;
        default:
            break;
    }
    component.setState({formErrors: fieldValidationErrors,
        emailValid: emailValid,
        passwordValid: passwordValid
    }, validateForm(component));
}

function validateForm(component) {
    component.setState({formValid: component.state.emailValid && component.state.passwordValid});
}

function errorClass(error) {
    return(error.length === 0 ? '' : 'has-error');
}