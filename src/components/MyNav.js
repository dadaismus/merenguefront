import React from 'react';
import { Collapse, Navbar, NavbarToggler, Nav, NavItem, Form, FormGroup, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import Main from './Main'

class MyNav extends React.Component {

    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render(){
        return (
                <Navbar color="light" light className="fixed-top">
                    <NavbarToggler onClick={this.toggleNavbar} className="m-0 p-0 border-0 dropdown-toggle">
                        <img src="https://merenguemerengue.com/wp-content/uploads/2017/12/logo-mm-1.png" height="38px" width="38px" className="rounded-circle"/>
                    </NavbarToggler>
                    <Form inline>
                        <FormGroup className="mt-1 mb-0">
                            <Input type="text" name="search" placeholder="Buscar..." />
                        </FormGroup>
                    </Form>
                    <Collapse isOpen={!this.state.collapsed} navbar>
                        <Nav navbar>
                            <NavItem>
                                <Link to="/tienda" className="nav-link" onClick={this.toggleNavbar}>Tienda</Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/login" className="nav-link" onClick={this.toggleNavbar}>Login</Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/registro" className="nav-link" onClick={this.toggleNavbar}>Registro</Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/registro/chef" className="nav-link" onClick={this.toggleNavbar}>Registro Chef</Link>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
        )
    }
}

export default MyNav;
