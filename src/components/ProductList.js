import React from 'react';
import Product from './Product';
import FilterButton from './FilterButton';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Col, Row
} from 'reactstrap';

class ProductList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Col>
                <Row>
                    <FilterButton/>
                </Row>
                <Row>
                    <Product/>
                    <Product/>
                    <Product/>
                    <Product/>
                </Row>
            </Col>
        );
    }
}

export default ProductList;