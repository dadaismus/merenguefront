import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container, ButtonGroup, Col, Row } from 'reactstrap';

 
 class FilterButton extends React.Component {

 	render(){
 		return(

		 	<Row>
		 		<Col xs="6">
				    <Button outline color="primary" block>Filtrar por</Button>
			     </Col>
			     <Col xs="6">
				    <Button outline color="primary" block>Ordenar por</Button>
				 </Col>
			</Row>

 	    );
 	} 

 }

 export default FilterButton;