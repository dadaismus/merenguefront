import React from 'react';
import MyNav from './MyNav.js';

class Header extends React.Component {

    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render(){
        return (
            <MyNav/>
        )
    }
}

export default Header;
