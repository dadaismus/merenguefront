import React from 'react';
import Login from './forms/Login';
import Register from './forms/Register';
import Product from './Product';
import FilterButton from './FilterButton';
import BtnFG from './forms/BtnFG'
import ProductList from './ProductList';
import { Switch, Route } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';

const Protected = () => <h3>Protected</h3>;

class Main extends React.Component {

    render(){
        return (
            <main className="h-100">
                <Switch>
                    <Route path="/productlist" component={ProductList} />
                    <Route path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                    
                    <Route path="/BtnFG" component={BtnFG} />
                    <Route path="/filterbtn" component={FilterButton} />
                    <PrivateRoute path="/protected" component={Protected}/>
                </Switch>
            </main>
        )
    }
}

export default Main;
